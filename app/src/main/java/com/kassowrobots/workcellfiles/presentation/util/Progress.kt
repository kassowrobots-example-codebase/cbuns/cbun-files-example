package com.kassowrobots.workcellfiles.presentation.util

/*
 * Progress data class represents the progress of various storage tasks. It contains both the
 * progress value (from 0 to 1) and progress message.
 */
data class Progress(
    val value: Float = 0f,
    val message: String = ""
)

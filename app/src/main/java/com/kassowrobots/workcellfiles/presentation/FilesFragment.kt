package com.kassowrobots.workcellfiles.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kassowrobots.api.app.fragment.KRFragment
import com.kassowrobots.workcellfiles.R
import com.kassowrobots.workcellfiles.presentation.util.FilesEvent

/*
 * FilesFragment is simply an entry point of Files CBunX application. The fragment is shown within
 * the Teach Pendant host app once the user clicks the Files application icon (Workcell -> Apps).
 * The fragment is responsible for the initialization of the UI view. In this tutorial, the Jetpack
 * Compose is used to build the UI view.
 */
class FilesFragment : KRFragment() {

    // Function onCreateView is responsible for the UI initialization of this fragment.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // ComposeView is an AndroidView that can host Jetpack Compose UI content. In this tutorial
        // the ComposeView represents the root view of this Files application fragment.
        return ComposeView(requireContext()).apply {
            // Function setViewCompositionStrategy is used to tell the compose to dispose itself
            // once the fragment is destroyed.
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnLifecycleDestroyed(viewLifecycleOwner)
            )
            // Function setContent sets the Jetpack Compose UI content for this ComposeView.
            setContent {
                // ViewModel is responsible for processing user input events within this fragment's
                // view and providing the view update.
                val viewModel = viewModel<FilesViewModel>()
                // InitViewModel event is sent in order to pass the instance of the KRContext into
                // the view model.
                viewModel.onEvent(FilesEvent.InitViewModel(krContext!!))

                // Surface is a composable that allows to specify the 'surface' parameters, such as
                // background color, elevation, clip or background shape. In this tutorial, the
                // Surface is used to modify the background color of this fragment.
                Surface(
                    color = Color(0xFFE2E6F0),
                    modifier = Modifier.fillMaxSize()
                ) {
                    // Column is a composable layout that places its children in a vertical sequence.
                    // In this tutorial, the modifier concept is used to define the padding, ie. the
                    // outer offset to be used on the content of the column. Modifier is simply an
                    // implementation of decorator pattern that can be used to modify the composable
                    // that it is applied to.
                    Column(
                        modifier = Modifier.padding(20.dp)
                    ) {
                        // Row is a composable layout that places its children in a horizontal sequence.
                        // In this tutorial, the row is used to place a file name input text side by
                        // side with upload button.
                        Row {
                            // TextInput is a composable that allows the user to enter text via
                            // virtual keyboard. In this case, the TextInput allows the user to enter
                            // the name of the file to be uploaded to the Workcell Files.
                            TextInput(
                                value = viewModel.state.fileName,
                                onValueChangeCallback = { newValue ->
                                    // OnFileNameChanged event is sent to view model each time the
                                    // user updates the filename.
                                    viewModel.onEvent(FilesEvent.OnFileNameChanged(newValue))
                                },
                                modifier = Modifier.weight(1f) // in order to occupy the whole remaining space
                            )
                            // Spacer represents an empty space layout. In this case it creates horizontal
                            // divider between the TextInput and KRButton with width of 5 dp.
                            Spacer(modifier = Modifier.width(5.dp))
                            // KRButton is custom composable that generates the labeled button and
                            // allows to define on click callback. See the KRButton composable definition
                            // bellow in this file.
                            KRButton(
                                label = "Upload",
                                onClickCallback = {
                                    // OnUploadClick event is sent to view model each time the user
                                    // clicks the upload button.
                                    viewModel.onEvent(FilesEvent.OnUploadClick)
                                },
                                modifier = Modifier.width(100.dp)
                            )
                        }
                        // LazyColumn is a composable layout that places its children in a vertical
                        // sequence. In contrast to pure Column composable, this one is optimized
                        // for dynamic content (ie. list of files).
                        LazyColumn {
                            // FileItem is generated for each file in state.files list.
                            viewModel.state.files.forEach { file ->
                                // FileItem is a custom lazy column item that shows the file name
                                // as well as the download and delete buttons. See the FileItem
                                // definition bellow in this file.
                                FileItem(
                                    file.name,
                                    onDownloadClick = {
                                        // OnDownloadClick event is sent to view model each time the
                                        // user clicks the download button. Note that the name of the
                                        // file to be downloaded is passed to the view model via this
                                        // event.
                                        viewModel.onEvent(FilesEvent.OnDownloadClick(file.name))
                                    },
                                    onDeleteClick = {
                                        // OnDeleteClick event is sent to view model each time the
                                        // user clicks the delete button. Note that the name of the
                                        // file to be deleted is passed to the view model via this
                                        // event.
                                        viewModel.onEvent(FilesEvent.OnDeleteClick(file.name))
                                    }
                                )
                            }
                        }
                        // The progress bar is shown only if the state.progress is available (non null).
                        viewModel.state.progress?.let { progress ->
                            // This time, the spacer is used as a vertical divider, since it is
                            // placed into a column and its height is specified.
                            Spacer(modifier = Modifier.height(12.dp))
                            // LinearProgressIndicator is a composable that shows a progress. By
                            // default the progress is defined from 0 (empty) to 1 (full).
                            LinearProgressIndicator(
                                progress = progress.value,
                                modifier = Modifier.fillMaxWidth()
                            )
                            // Text is a composable that displays the plain text. In this case, the
                            // Text is used to show a progress message directly bellow the progress
                            // bar.
                            Text(
                                text = progress.message,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )
                        }
                        // The error is shown only if the state.error is available (non null).
                        viewModel.state.error?.let { error ->
                            // Another vertical divider.
                            Spacer(modifier = Modifier.height(12.dp))
                            // Another text composable. This time is it used to show the error
                            // message. Therefore the text color is set to red.
                            Text(
                                text = error,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center,
                                color = Color.Red
                            )
                        }
                        // Another vertical divider.
                        Spacer(modifier = Modifier.height(12.dp))
                        if (viewModel.state.content.isNotEmpty()) {
                            // Another text composable. This one is used for the content of the
                            // downloaded file.
                            Text(
                                text = viewModel.state.content,
                                modifier = Modifier.fillMaxSize()
                                    .verticalScroll(rememberScrollState())
                                    .background(color = Color.White)
                                    .padding(8.dp)
                            )
                        }
                    }
                }
            }
        }
    }

    /*
     * Custom composable that allows the user to enter a text input via a virtual keyboard. It
     * simply wraps the Jetpack Compose BasicTextField and defines its behaviour and some of its
     * parameters. You can pass an input text and onValueChange callback as a parameters of the
     * TextInput.
     */
    @Composable
    fun TextInput(
        value: String,
        onValueChangeCallback: (String) -> Unit,
        modifier: Modifier = Modifier,
        enabled: Boolean = true
    ) {
        val focusManager = LocalFocusManager.current
        BasicTextField(
            value = value,
            modifier = modifier
                .height(50.dp)
                .background(
                    color = Color.White,
                    shape = RoundedCornerShape(5.dp)
                )
                .padding(16.dp),
            onValueChange = {
                onValueChangeCallback(it)
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus(false)
                }
            ),
            textStyle = TextStyle.Default.copy(
                fontSize = 14.sp,
                color = if (enabled) Color(0xFF393B3F) else Color(0xFF9A9A9A),
                fontFamily = FontFamily.Monospace
            ),
            enabled = enabled
        )
    }

    /*
     * Custom composable that defines the white labeled button with rounded corners. It simply wraps
     * the Jetpack Compose Button with Text inside and defines the UI of the button. You can pass
     * the label and the onClick callback as a parameters of the KRButton.
     */
    @Composable
    fun KRButton(
        label: String,
        onClickCallback: () -> Unit,
        modifier: Modifier = Modifier,
        enabled: Boolean = true
    ) {
        Button(
            onClick = onClickCallback,
            enabled = enabled,
            shape = RoundedCornerShape(5.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.White,
                disabledBackgroundColor = Color.White
            ),
            modifier = modifier
                .height(50.dp)
                .background(
                    color = Color.White,
                    shape = RoundedCornerShape(5.dp)
                )
        ) {
            Text(
                text = label,
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold,
                color =  if (enabled) Color(0xFF616265) else Color(0xFFC9CBCD),
            )
        }
    }

    /*
     * Custom lazy column item that represents a single Workcell file. The item contains file name
     * as well as download and delete file buttons. It simply wraps a LazyListScope item with
     * the composable definition inside.
     */
    fun LazyListScope.FileItem(
        filaName: String,
        onDownloadClick: () -> Unit,
        onDeleteClick: () -> Unit
    ) {
        item {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = filaName,
                    fontSize = 14.sp,
                    color = Color(0xFF393B3F),
                    fontFamily = FontFamily.Monospace,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )
                Spacer(modifier = Modifier.weight(1f))
                IconButton(
                    onClick = onDownloadClick,
                    modifier = Modifier.size(50.dp)
                ) {
                    Icon(
                        painterResource(R.drawable.ic_download),
                        "download"
                    )
                }
                IconButton(
                    onClick = onDeleteClick,
                    modifier = Modifier.size(50.dp)
                ) {
                    Icon(
                        painterResource(R.drawable.ic_delete),
                        "delete"
                    )
                }
            }
            Divider(color = Color.Gray)
        }
    }

}
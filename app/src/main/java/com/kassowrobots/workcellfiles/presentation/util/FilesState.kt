package com.kassowrobots.workcellfiles.presentation.util

import com.kassowrobots.api.robot.storage.StorageRef

/*
 * FilesState represents the state of the UI, ie. all that should be visible to the user. It
 * serves for the ViewModel -> View communication. Once the state data are updated, also the views
 * that access these data are updated. In this tutorial the FilesState is used to provide the list
 * of available files, requested upload file name as well as progress and error of various storage
 * tasks.
 */
data class FilesState(
    val files: List<StorageRef> = listOf(), // list of available Workcell files
    val fileName: String = "my_file.txt",   // user defined name of the file to be uploaded
    val progress: Progress? = null,         // progress of the upload/download and delete storage tasks
    val error: String? = null,              // latest storage task error
    val content: String = ""                // content of the downloaded file
)

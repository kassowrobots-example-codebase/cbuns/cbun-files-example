package com.kassowrobots.workcellfiles.presentation.util

import com.kassowrobots.api.app.KRContext

/*
 * FilesEvent is a base interface for all UI input events, ie. all events to be triggered on user
 * interaction. Each input event is represented by a single subclass of FilesEvent. It serves
 * for the View -> ViewModel communication. Once the user interacts with the UI, corresponding
 * event is sent from the view to the view model.
 */
sealed interface FilesEvent {

    // Event to be sent once the application is opened.
    data class InitViewModel(val context: KRContext): FilesEvent

    // Event to be sent when user clicks the download file button.
    data class OnDownloadClick(val filename: String): FilesEvent

    // Event to be sent when user clicks the delete file button.
    data class OnDeleteClick(val filename: String): FilesEvent

    // Event to be sent on each update of the upload file name.
    data class OnFileNameChanged(val fileName: String): FilesEvent

    // Event to be sent when user clicks the upload file button.
    object OnUploadClick: FilesEvent

}
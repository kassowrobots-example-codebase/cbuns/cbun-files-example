package com.kassowrobots.workcellfiles.presentation

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.kassowrobots.workcellfiles.domain.DeleteFileUseCase
import com.kassowrobots.workcellfiles.domain.DownloadFileUseCase
import com.kassowrobots.workcellfiles.domain.ListFilesUseCase
import com.kassowrobots.workcellfiles.domain.UploadFileUseCase
import com.kassowrobots.workcellfiles.presentation.util.FilesEvent
import com.kassowrobots.workcellfiles.presentation.util.FilesState
import com.kassowrobots.workcellfiles.presentation.util.Progress
import kotlinx.coroutines.launch
import java.io.File

/*
 * Represents ViewModel for the FilesFragment view. It is inherited from the Android
 * ViewModel. This class processes user interaction (in form of FilesEvent) and provides the UI
 * refresh (via mutable FilesState).
 */
class FilesViewModel(application: Application): AndroidViewModel(application) {

    // Mutable FilesState to be used for the UI update. The state itself has no access modifier,
    // ie. it is public by default (in order to be accessible from the view). But it implements
    // the property setter with private access modifier to restrict the state modification from the
    // outside of this class.
    var state by mutableStateOf(FilesState())
        private set

    // ListFilesUseCase contains business logic that retrieves the list of available Workcell files.
    // The lateinit modifier allows to initialize the member outside of the constructor. In this case
    // the listFiles member is initialized on InitViewModel event.
    lateinit var listFiles: ListFilesUseCase

    // UploadFileUseCase contains business logic that uploads a local file to the robot storage.
    lateinit var uploadFile: UploadFileUseCase

    // DownloadFileUseCase contains business logic that downloads a file from the robot storage.
    lateinit var downloadFile: DownloadFileUseCase

    // DeleteFileUseCase contains business logic that removes a file from the robot storage.
    lateinit var deleteFile: DeleteFileUseCase

    /*
     * This method processes the user interaction events and provides the appropriate response. Each
     * interaction event is represented by a single subclass of FilesEvent.
     */
    fun onEvent(event: FilesEvent) {
        when (event) {
            // InitViewModel event is sent once the Fragment view is created. KRContext instance is
            // used for the initialization of the use cases.
            is FilesEvent.InitViewModel -> {
                listFiles = ListFilesUseCase(event.context)
                uploadFile = UploadFileUseCase(event.context)
                downloadFile = DownloadFileUseCase(event.context)
                deleteFile = DeleteFileUseCase(event.context)

                // New coroutines is launched to fetch the list of available files.
                viewModelScope.launch {
                    fetchFiles()
                }
            }

            // If the name of the file to be uploaded is changed, the state is updated.
            is FilesEvent.OnFileNameChanged -> {
                state = state.copy(fileName = event.fileName)
            }

            // If the upload button is clicked, the upload file use case is invoked.
            FilesEvent.OnUploadClick -> {
                // Resets the progress bar to start with 0%.
                resetProgressBar()
                // New coroutine is launched for the upload file use case invocation.
                viewModelScope.launch {
                    // Upload file use case is invoked with the requested file name. The collect
                    // callback is invoked each time the use case emits a snapshot.
                    uploadFile(state.fileName, 10000).collect {
                        // Snapshot data are used to update the progress and error UI.
                        val message = "${(it.progress * 100).toInt()} %"
                        state = state.copy(
                            progress = Progress(it.progress, message),
                            error = it.error?.message
                        )
                        // Once the task is complete, the list of available files is refreshed.
                        if (it.progress == 1f) {
                            fetchFiles()
                        }
                    }
                }
            }

            // If the file download button is clicked, the download file use case is invoked.
            is FilesEvent.OnDownloadClick -> {
                // Resets the progress bar to start with 0%.
                resetProgressBar()
                // New coroutine is launched for the download file use case invocation.
                viewModelScope.launch {
                    val target = File(getApplication<Application>().filesDir, event.filename)
                    // Download file use case is invoked with the requested file name. The collect
                    // callback is invoked each time the use case emits a snapshot.
                    downloadFile(event.filename, target).collect {
                        // Snapshot data are used to update the progress and error UI.
                        val message = "${(it.progress * 100).toInt()} %"
                        state = state.copy(
                            progress = Progress(it.progress, message),
                            error = it.error?.message
                        )
                        // Once the task is complete, the UI state.content is updated with the
                        // data of the downloaded file.
                        if (it.progress == 1f) {
                            // First 5 lines of the downloaded file are read and joined to the
                            // string content.
                            val content: String = target.bufferedReader()
                                .useLines { lines: Sequence<String> ->
                                    lines
                                        .take(5)
                                        .joinToString()
                                }
                            // The content string is used to update the content UI.
                            state = state.copy(
                                content = content,
                                progress = null
                            )
                        }
                    }
                }
            }

            // If the delete file button is clicked, the delete file use case is invoked.
            is FilesEvent.OnDeleteClick -> {
                // Resets the progress bar to start with 0%.
                resetProgressBar()
                // New coroutine is launched for the download file use case invocation.
                viewModelScope.launch {
                    // Delete file use case is invoked with the requested file name. The collect
                    // callback is invoked each time the use case emits a snapshot.
                    deleteFile(event.filename).collect {
                        // Snapshot data are used to update the progress and error UI.
                        val message = "${(it.progress * 100).toInt()} %"
                        state = state.copy(
                            progress = Progress(it.progress, message),
                            error = it.error?.message
                        )
                        // Once the task is complete, the list of available files is refreshed.
                        if (it.progress == 1f) {
                            fetchFiles()
                        }
                    }
                }
            }

        }

    }

    /*
     * Resets the progress bar to 0%. Above that it also clears the error and download file content.
     */
    private fun resetProgressBar() {
        state = state.copy(
            progress = Progress(0f, "0%"),
            content = "",
            error = null
        )
    }

    /*
     * Invokes list files use case to retrieve a list of available Workcell files. The result is
     * used to update the UI state.files. In addition, the progress bar is hidden.
     */
    private suspend fun fetchFiles() {
        listFiles().collect { files ->
            state = state.copy(
                progress = null,
                files = files
            )
        }
    }

}
package com.kassowrobots.workcellfiles.domain

import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.robot.storage.FilesStorage
import com.kassowrobots.api.robot.storage.UploadTask
import com.thedeanda.lorem.LoremIpsum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.File
import java.io.FileOutputStream

/*
 * UploadFileUseCase contains business logic that transfer a local file to the robot storage.
 */
class UploadFileUseCase(
    private val context: KRContext
) {

    operator fun invoke(fileName: String, paragraphs: Int): Flow<UploadTask.TaskSnapshot> = flow {
        // Generates the "Lorem Ipsum" local file to be uploaded to the robot storage.
        val rootDir = context.filesDir
        val file = File(rootDir, fileName)
        FileOutputStream(file).use {
            it.write(LoremIpsum.getInstance().getParagraphs(paragraphs, paragraphs).toByteArray())
        }
        // Retrieves FileStorage service from the KRContext.
        val filesStorage = context.getKRService(KRContext.FILES_STORAGE_SERVICE) as? FilesStorage
        // Starts the upload of the local file to the robot storage.
        val uploadTask = filesStorage?.upload(file)
        uploadTask?.let { task ->
            // Periodically emits the task state until the task is complete.
            do {
                delay(50)
                emit(uploadTask.snapshot)
            } while (!task.isComplete)
            // Emits the task result (if there is no error).
            if (task.error == null) {
                emit(uploadTask.result)
            }
        }
    }.flowOn(Dispatchers.Default) // Configures the flow to be launched on default (background) thread.

}
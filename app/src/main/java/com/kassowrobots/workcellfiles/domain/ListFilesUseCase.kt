package com.kassowrobots.workcellfiles.domain

import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.robot.storage.FilesStorage
import com.kassowrobots.api.robot.storage.StorageRef
import com.kassowrobots.api.util.KRLog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/*
 * ListFilesUseCase contains business logic that retrieves the list of available Workcell files.
 */
class ListFilesUseCase(
    private val context: KRContext
) {

    operator fun invoke(): Flow<List<StorageRef>> = flow {
        // Retrieves FileStorage service from the KRContext.
        val filesStorage = context.getKRService(KRContext.FILES_STORAGE_SERVICE) as? FilesStorage
        val task = filesStorage?.listFiles()
        task?.let {
            // Waits until the list files task is complete.
            do {
                delay(50)
            } while (!task.isComplete)

            // Emits the task result (if there is no error).
            if (it.error == null) {
                emit(task.result.files)
            } else {
                KRLog.e("ListFilesUseCase", "Failed to list files: " + it.error)
            }
        }
    }.flowOn(Dispatchers.Default) // Configures the flow to be launched on default (background) thread.

}
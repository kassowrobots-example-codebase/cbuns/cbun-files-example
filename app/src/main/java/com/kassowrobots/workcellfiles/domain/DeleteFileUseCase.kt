package com.kassowrobots.workcellfiles.domain

import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.robot.storage.DeleteTask
import com.kassowrobots.api.robot.storage.FilesStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/*
 * DeleteFileUseCase contains business logic that removes a file from the robot storage.
 */
class DeleteFileUseCase(
    private val context: KRContext
) {

    operator fun invoke(fileName: String): Flow<DeleteTask.TaskSnapshot> = flow {
        // Retrieves FileStorage service from the KRContext.
        val filesStorage = context.getKRService(KRContext.FILES_STORAGE_SERVICE) as? FilesStorage
        // Obtains the child (by its filename) and starts the delete task.
        val task = filesStorage?.getChild(fileName)?.delete()
        task?.let {
            // Periodically emits the task state until the task is complete.
            do {
                delay(50)
                emit(task.snapshot)
            } while (!task.isComplete)
            // Emits the task result (if there is no error).
            if (it.error == null) {
                emit(task.result)
            }
        }
    }.flowOn(Dispatchers.Default) // Configures the flow to be launched on default (background) thread.

}
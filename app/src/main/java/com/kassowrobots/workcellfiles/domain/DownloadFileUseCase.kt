package com.kassowrobots.workcellfiles.domain

import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.robot.storage.DownloadTask
import com.kassowrobots.api.robot.storage.FilesStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.File

/*
 * DownloadFileUseCase contains business logic that transfers a file from the robot storage to the
 * local (tablet) storage.
 */

class DownloadFileUseCase(
    private val context: KRContext
) {

    operator fun invoke(fileName: String, target: File): Flow<DownloadTask.TaskSnapshot> = flow {
        // Retrieves FileStorage service from the KRContext.
        val filesStorage = context.getKRService(KRContext.FILES_STORAGE_SERVICE) as? FilesStorage
        // Obtains the child (by its name) and starts the download task.
        val downloadTask = filesStorage?.getChild(fileName)?.download(target)
        downloadTask?.let { task ->
            // Periodically emits the task state until the task is complete.
            do {
                delay(50)
                emit(downloadTask.snapshot)
            } while (!task.isComplete)
            // Emits the task result (if there is no error).
            if (task.error == null) {
                emit(downloadTask.result)
            }
        }
    }.flowOn(Dispatchers.Default) // Configures the flow to be launched on default (background) thread.

}
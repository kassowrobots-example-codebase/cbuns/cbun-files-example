# Files Example CBun

Files Example CBun demonstrates the usage of CBunX API to manage Workcell Files (ie. the files that 
can be added to the Workcell and used later during the program execution from various CBuns). The 
CBun UI shows the list of available Workcell files. The user can generate a new (lorem ipsum) file 
with the given name by clicking the **Upload** button. Clicking the **download** icon button downloads 
the file from robot storage to the tablet and shows the first 5 lines of the file content. The user 
can also remove the file from Workcell by clicking the **delete** icon button.

![Files Example CBun](/printscreen.png)

The Files Example CBun is written in Kotlin and based on the [CBunX Framework](https://kassowrobots.gitlab.io/cbunx-api-doc/).

## Frontend Build Environment

The Files Example CBun comes with the preconfigured frontend build environment which is based on the Android Studio. With this you can open and debug the CBun frontend on Windows, MacOS or Linux without the need for complicated build environment configuration. 

### Prerequisities

To get started, follow these steps:

1. Install the [Android Studio](https://developer.android.com/studio).

### Frontend Emulator Setup

By using the emulator (virtual device) you can test your UI directly on your development machine within the Android Studio. You need to configure the virtual device just once for all of your CBun frontend projects. To setup the emulator, follow these steps.

1. Open the CBun project folder in Android Studio. 

2. Open **Tools** -> **Device Manager** to configure an emulated virtual device for the standalone application test. Click **Create device** to add new virtual device.

3. Click **New Hardware Profile** to create new emulator profile, that will match the UI behaviour of the real robot in terms of the resolution and the density of pixels.

4. Enter the device name (for example CBun App Emulator) and configure the emulator resolution (932 x 987 px) and screen size (6.1”) to fit the CBun App container in the Teach Pendant host app. The click the **Finish** button.

5. Once the new hardware profile is added, select it and click the **Next** button.

6. Select the **Q** system image, since Android 10 ensures compatibility with the highest amount of our robots. Then click the **Next** button.

7. Check the device configuration, enter the AVD Name and click the **Finish** button.

8. Finally click the run app button (or Ctrl-R shortcut) to launch your application in the emulator.

## CBun Assembly

The CBun installer is generated automatically in the root project folder.
The CBun assemble bash script (assemble_cbun.sh) is hooked on the project debug build task via the app module
gradle script. Once the CBun is generated, it can be installed on the real robot from a USB stick or Google Drive.

Follow the steps bellow in order to generate and install the CBun:

1. Open the CBun project folder in Android Studio
2. Set **debug** build variant (**Build** -> **Select Build Variant...** -> **debug**)
3. Build the project (**Build** -> **Build Bundle(s)/APK(s)** -> **Build APK(s)**)
4. Copy the CBun (**PROJECT_FOLDER/workcellfiles.cbun**) to a USB stick
5. Install the CBun on a real robot from the USB stick

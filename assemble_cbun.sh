#!/bin/bash

mkdir assemble
cd assemble
mkdir cbun_workcell_files
cp ../bundle.xml ./cbun_workcell_files
cp ../app/build/outputs/apk/debug/app-debug.apk ./cbun_workcell_files/files.apk
tar --exclude='*.DS_Store' -czvf workcellfiles.cbun -C ./cbun_workcell_files .
cp workcellfiles.cbun ../
cd ..
rm -rf assemble